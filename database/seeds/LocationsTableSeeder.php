<?php

use Illuminate\Database\Seeder;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locations = ['Центар', 'Аеродром', 'Карпош', 'Кисела Вода', 'Ѓорче Петров', 'Автокоманда'];

	    foreach ($locations as $location) {
	    	    DB::table('locations')->insert([
	            'name' => $location
	        ]);
	    }

	}
}
