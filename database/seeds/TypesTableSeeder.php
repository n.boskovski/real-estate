<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = ['стан', 'куќа', 'деловен простор', 'плац'];

	    foreach ($types as $type) {
	    	    DB::table('types')->insert([
	            'name' => $type
	        ]);
	    }
    }
}
