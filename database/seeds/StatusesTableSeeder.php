<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = ['се продава', 'се издава'];

	    foreach ($statuses as $status) {
	    	    DB::table('statuses')->insert([
	            'name' => $status
	        ]);
	    }
    }
}
