<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $nam;
    public $num;
    public $ema;
    public $sub;
    public $mes;
    


    public function __construct($name, $number, $email, $subject, $message)
    {
        $this->nam = $name;
        $this->num = $number;
        $this->ema = $email;
        $this->sub = $subject;
        $this->mes = $message;        
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $e_name = $this->nam;
        $e_number = $this->num;
        $e_email = $this->ema;
        $e_subject = $this->sub;       
        $e_message = $this->mes;



        return $this->view('email', compact("e_name", "e_number", "e_email", "e_subject", "e_message"))->subject($e_subject);


    }
}
