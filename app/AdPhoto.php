<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdPhoto extends Model
{

	protected $fillable = ['url', 'ad_id'];

    public function ad(){
        return $this->belongsTo('App\Ad');
    }
}
