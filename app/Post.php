<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

	protected $fillable = ['title', 'featured', 'content', 'slug'];

    public function getFeaturedAttribute($featured){
        return asset($featured);
    }

    public function tags(){
    	return $this->belongsToMany('App\Tag');
    }
}
