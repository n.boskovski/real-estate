<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{

	protected $fillable = ['title', 'content', 'featured', 'price', 'area', 'location_id', 'type_id', 'status_id', 'bedrooms', 'bathrooms', 'slug'];


    public function location(){
    	return $this->belongsTo('App\Location');
    }
    public function status(){
    	return $this->belongsTo('App\Status');
    }
    public function type(){
    	return $this->belongsTo('App\Type');
    }


    public function getFeaturedAttribute($featured){
        return asset($featured);
    }


    public function photos(){
        return $this->hasMany('App\AdPhoto');
    }
}
