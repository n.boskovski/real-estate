<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Session;
use App\Tag;
use App\Post;
use File;


class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.posts.index')->with('posts', Post::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.posts.create')->with('tags', Tag::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'title' => 'required',
            'featured' => 'required|image',
            'content' => 'required'
            // 'tags' => 'required'
        ]);

        $slug = str_slug($request->title);

        $posts_count = Post::all()->count()+1;
        if(Post::whereSlug($slug)->exists()){
            $slug = $slug.'-'.$posts_count;
        }

        $featured = $request->featured;
        $featured_new_name = time().$featured->getClientOriginalName();
        $featured_new_name = time().$featured->getClientOriginalName();


        $featured->move('uploads/posts', $featured_new_name);

        $post = Post::create([
            'title' => $request->title,
            'slug' => $slug,
            'featured' => 'uploads/posts/'.$featured_new_name,
            'content' => $request->content
        ]);

        // dd($post->featured);

        $post->tags()->attach($request->tags);

        $post->save();

        Session::flash('success', 'Успешно креиравте блог-пост');

        return redirect()->route('posts')->with('posts', Post::all());

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.posts.edit')->with('post', Post::find($id))
                                        ->with('tags', Tag::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $post = Post::find($id);

        $this->validate($request, [
            // 'title' => 'required',
            'featured' => 'image',
            'content' => 'required'
            // 'tags' => 'required'
        ]);

        $string = $post->featured;
        $words_array = (explode("/",$string));
        $last = end($words_array);

        if ($request->hasFile('featured')) {
            
            $image_path = "uploads/posts/".$last;
            if(File::exists($image_path)) {
            File::delete($image_path);
            }

            $featured = $request->featured;
            $featured_new_name = time().$featured->getClientOriginalName();
            $featured->move('uploads/posts', $featured_new_name);
            $post->featured = '/uploads/posts/'.$featured_new_name;               
        }

        $post->content = $request->content;
        $post->tags()->sync($request->tags);
        $post->save();

        if ($request->title) {

            $slug = str_slug($request->title);
            $posts_count = Post::all()->count()+1;

            if(Post::whereSlug($slug)->exists()){
                $slug = $slug.'-'.$posts_count;
            }

            $post->slug = $slug;
            $post->title = $request->title;
            $post->save();
        }

        Session::flash('success', 'Успешно променивте блог-пост');

        return redirect()->route('posts');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        $string = $post->featured;
        $words_array = (explode("/",$string));
        $last = end($words_array);

        $image_path = "uploads/posts/".$last;

        if(File::exists($image_path)) {
            File::delete($image_path);
        }


        $post->delete();

        Session::flash('success', 'Успешно избришавте блог-пост');

        return redirect()->route('posts');
    }
}
