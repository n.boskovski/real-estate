<?php

namespace App\Http\Controllers;

use App\Location;

use Illuminate\Http\Request;

use Session;

use App\Ad;

class LocationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.locations.index')->with('locations', Location::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.locations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        $this->validate($request, [
            'location' => 'required|string'
        ]);

        $location = new Location;

        $location->name = $request->location;

        $location->save();

        Session::flash('success', 'Успешно креиравте локација');

        return redirect()->route('locations');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $location = Location::find($id);

        return view('admin.locations.edit')->with('location', $location);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'location' => 'required'
        ]);

        $location = Location::find($id);
        $location->name = $request->location;
        $location->save();

        Session::flash('success', 'Успешно променивте локација');


        return redirect()->route('locations');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $ads = Ad::where('location_id', $id)->get();

        if ($ads->count()>=1) {

            Session::flash('error', 'Не можете да ја избришете оваа локација');
            
            return redirect()->back();
        }

        $location = Location::find($id);
        $location->delete();

        Session::flash('success', 'Успешно избришавте локација');

        return redirect()->route('locations');
    }
}