<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ad;
use App\Location;
use App\Type;
use App\Status;
use App\Post;
use App\Tag;
use Mail;
use App\Mail\SendMail;


class FrontEndController extends Controller
{
    public function index(){
    	return view('index')
    					->with('last_six_lists', Ad::orderBy('created_at','desc')->take(6)->get())
    					->with('locations', Location::all())
                        ->with('statuses', Status::all())
                        ->with('types', Type::all())
                        ->with('last_three_posts', Post::orderBy('created_at','desc')->take(3)->get()); 
    }

    public function blog(){
    	return view('blog')
    					->with('blog_posts', Post::orderBy('created_at', 'desc')->paginate(6));
    }

    public function contact(){
    	return view('contact');
    }

    public function allads(){

        $sta = null;
        $loc = null;
        $typ = null;
        $bed = null;
        $bat = null;

        return view('allads')
                        ->with('sta', $sta)       
                        ->with('loc', $loc)       
                        ->with('typ', $typ)       
                        ->with('bed', $bed)       
                        ->with('bat', $bat) 
    					->with('ads', Ad::orderBy('created_at', 'desc')->get())
                        ->with('locations', Location::all())
                        ->with('statuses', Status::all())
                        ->with('types', Type::all());
    }
    
    public function aboutUs(){
        return view('about');
    }

    public function singlePost($slug){



    	$post = Post::where('slug', $slug)->first();


        if ($post === null) {

            abort(404);
        }

    	return view('singlepost')
    					->with('post', $post);
    }

    public function singleAd($slug){

        $last_four_ads = Ad::orderBy('created_at','desc')->take(4)->get();

        $ad = Ad::where('slug', $slug)->first();

        if ($ad === null) {

            abort(404);
        }

        $images = $ad->photos()->get();


        return view('singlead')
                        ->with('ad', $ad)
                        ->with('last_four_ads', $last_four_ads)
                        ->with('locations', Location::all())
                        ->with('statuses', Status::all())
                        ->with('types', Type::all())
                        ->with('images', $images);
    }

    public function sendMail(Request $request){

        $this->validate($request, [

            'name' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
            'number' => 'required',
            'message' => 'required'

        ]);

        $name = $request->name;
        $email = $request->email;
        $number = $request->number;
        $subject = $request->subject;
        $message = $request->message;


        Mail::to('info@makom.mk')->send(new SendMail($name, $number, $email, $subject, $message));

        return back()->with('success', 'Ви благодариме што не контактиравте!');

    }


    public function results(Request $request){


    	$query = Ad::query();

        if($request->status !== null){ 
            $query->where('status_id', $request->status);
        }

        if($request->location !== null){ 
            $query->where('location_id', $request->location);
        }

        if($request->type !== null){ 
            $query->where('type_id', $request->type);
        }

    	if($request->bedrooms !== null){ 
    		$query->where('bedrooms', $request->bedrooms);
    	}

        if($request->bathrooms !== null){ 
            $query->where('bathrooms', $request->bathrooms);
        }

        $sta = $request->status;
        $loc = $request->location;
        $typ = $request->type;
        $bed = $request->bedrooms;
        $bat = $request->bedrooms;

        return view('allads')
                        ->with('sta', $sta)       
                        ->with('loc', $loc)       
                        ->with('typ', $typ)       
                        ->with('bed', $bed)       
                        ->with('bat', $bat)       
                        ->with('locations', Location::all())
                        ->with('statuses', Status::all())
                        ->with('types', Type::all())
                        ->with('ads', $ads = $query->orderBy('created_at', 'desc')->get());
    }

    public function resultsajax(Request $request){

        $query = Ad::query();

        if($request->status !== null){ 
            $query->where('status_id', $request->status);
        }

        if($request->location !== null){ 
            $query->where('location_id', $request->location);
        }

        if($request->type !== null){ 
            $query->where('type_id', $request->type);
        }

        if($request->bedrooms !== null){ 
            $query->where('bedrooms', $request->bedrooms);
        }

        if($request->bathrooms !== null){ 
            $query->where('bathrooms', $request->bathrooms);
        }

        $ads = $query->orderBy('created_at', 'desc')->get();



        
        $adArray = [];  

        foreach($ads as $ad){

            $bedrooms = "";
            $bathrooms = "";
            
            if(!empty ($ad->bedrooms)) {
                if($ad->bathrooms>1) {
                   $bedrooms = ' <p><i class="fa fa-bed"></i> '.$ad->bedrooms.' спални</p> ';
                } else {
                    $bedrooms = ' <p><i class="fa fa-bed"></i> '.$ad->bedrooms.' спална</p> ';
                }
            }
            if(!empty ($ad->bathrooms)) {
                if($ad->bathrooms>1) {
                   $bathrooms =  '<p><i class="fa fa-bath"></i> '. $ad->bathrooms . ' тоалети</p>';
                }else {
                    $bathrooms =  '<p><i class="fa fa-bath"></i> '. $ad->bathrooms . ' тоалет</p>';
                }
                
            }

        $ad = '<div class="col-md-4 col-sm-6 list-item list-item-cover remove-lists" style="display:none"> 
                <img src="' . $ad->featured . '" alt="">
                <div class="list-text">
                    <div class="text-center list-title">
                        <h1>
                            <a href="/ad/' . $ad->slug . ' "> ' . $ad->title . '</a>
                        </h1>
                        <p><i class="fas fa-map-marker-alt"></i> ' . $ad->location->name . '</p>
                    </div>
                    <div class="list-info">
                        <p><i class="fa fa-th-large"></i> ' . $ad->area . ' квадрати</p> ' .
                        $bedrooms . $bathrooms . '
                    </div>
                    <div class="list-price">
                    <a href="/ad/' . $ad->slug . ' "> ' . $ad->price . '</a>
                    </div>                  
                </div>
            </div>';

        array_push($adArray , $ad);

        }

        return $adArray;
    }




    public function tag($slug){

        return view('tag')->with('tag', $tag = Tag::where('slug', '=', $slug)->first());
    }

}
