<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Session;
use App\Location;
use App\Status;
use App\Type;
use App\Ad;
use App\AdPhoto;
use File;


class AdsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ads = Ad::all();

        return view('admin.ads.index')->with('ads', $ads);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $locations = Location::all();


        if ($locations->count() == 0) {
            
            Session::flash('info', 'Мора да имате внесено локации пред да креирате оглас');

            return redirect()->back();

        }

        return view ('admin.ads.create')->with('locations', Location::all())
                                        ->with('statuses', Status::all())
                                        ->with('types', Type::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd($request->all());


        $this->validate($request, [
            'title' => 'required',
            'featured' => 'required|image',
            'content' => 'required',
            'price' => 'required|integer',
            'area' => 'required|integer',
            'bedrooms' => '|nullable|integer|min:0|max:5',
            'bathrooms' => '|nullable|integer|min:0|max:5',
            'location_id' => 'required',
            'status_id' => 'required',
            'type_id' => 'required'
        ]);


        $featured = $request->featured;
        $featured_new_name = time().$featured->getClientOriginalName();
        $featured->move('uploads/lists', $featured_new_name);

        $slug = str_slug($request->title);

        $ads_count = Ad::all()->count()+1;

        if(Ad::whereSlug($slug)->exists()){
            $slug = $slug.'-'.$ads_count;
        }


        $ad = Ad::create([
            'title' => $request->title,
            'slug' => $slug,
            'featured' => 'uploads/lists/'.$featured_new_name,
            'content' => $request->content,
            'price' => $request->price,
            'area' => $request->area,
            'bedrooms' => $request->bedrooms,
            'bathrooms' => $request->bathrooms,
            'location_id' => $request->location_id,
            'status_id' => $request->status_id,
            'type_id' => $request->type_id
        ]);


        if($request->hasFile('photos')){

            $ad_id = $ad->id;

            foreach ($request->photos as $photo) {                
                $photo_new_name = time(). uniqid().$photo->getClientOriginalName();
                $photo->move('uploads/adphotos', $photo_new_name);

                $adphoto = AdPhoto::create([
                    'ad_id' =>  $ad_id,
                    'url' => 'uploads/adphotos/'.$photo_new_name
                ]);
            }

        }

        Session::flash('success', 'Успешно внесовте оглас');

        return redirect()->route('ads'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ad = Ad::find($id);

        return view('admin.ads.edit')->with('ad', $ad)
                                    ->with('types', Type::all())
                                    ->with('statuses', Status::all())
                                    ->with('locations', Location::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        // dd($request->all());

        $ad = Ad::find($id);

        $this->validate($request, [
            'featured' => 'image',
            'content' => 'required',
            'price' => 'required|integer',
            'area' => 'required|integer',
            'bedrooms' => '|nullable|integer|min:0|max:5',
            'bathrooms' => '|nullable|integer|min:0|max:5',
            'location_id' => 'required',
            'status_id' => 'required',
            'type_id' => 'required'
        ]);



        $string = $ad->featured;

        $words_array = (explode("/",$string));
        $last = end($words_array);


        if ($request->hasFile('featured')) { 

            $image_path = "uploads/lists/".$last;
            
            if(File::exists($image_path)) {
                File::delete($image_path);
            }

            $featured = $request->featured;
            $featured_new_name = time().$featured->getClientOriginalName();
            $featured->move('uploads/lists', $featured_new_name);
            $ad->featured =  '/uploads/lists/'.$featured_new_name;               
        }


        if($request->hasFile('photos')){


            $ad_id = $ad->id;


            foreach ($request->photos as $photo) {                
                $photo_new_name = time(). uniqid().$photo->getClientOriginalName();
                $photo->move('uploads/adphotos', $photo_new_name);

                $adphoto = AdPhoto::create([
                    'ad_id' =>  $ad_id,
                    'url' => 'uploads/adphotos/'.$photo_new_name
                ]);
            }

        }

        $ad->content = $request->content;
        $ad->price = $request->price;
        $ad->area = $request->area;
        $ad->bedrooms = $request->bedrooms;
        $ad->bathrooms = $request->bathrooms;
        $ad->location_id = $request->location_id;
        $ad->status_id = $request->status_id;
        $ad->type_id = $request->type_id;

        $ad->save();

        if ($request->title) {

            $slug = str_slug($request->title);
            $ads_count = Ad::all()->count()+1;        

            if(Ad::whereSlug($slug)->exists()){
                $slug = $slug.'-'.$ads_count;
            }

            $ad->slug = $slug;
            $ad->title = $request->title;
            $ad->save();
        }

        Session::flash('success', 'Успешно променивте оглас');

        return redirect()->route('ads'); 

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $ad = Ad::find($id);

        $images = $ad->photos()->get();

        foreach ($images as $image) {
            
            $string = $image->url;
            $words_array = (explode("/",$string));
            $last = end($words_array);

            $image_path = "uploads/adphotos/".$last;

            if(File::exists($image_path)) {
                File::delete($image_path);
            }

            $image->delete();
        }

        $string = $ad->featured;

        $words_array = (explode("/",$string));
        $last = end($words_array);

        $image_path = "uploads/lists/".$last;
        
        if(File::exists($image_path)) {
            File::delete($image_path);
        }


        $ad->delete();

        Session::flash('success', 'Успешно избришавте оглас');

        return redirect()->route('ads'); 
    }

    public function search(){
        
        return view('admin.ads.search');
    }

    public function result(Request $request){

        $this->validate($request, [
            'id' => 'required|integer'
        ]);

        $ad = Ad::find($request->id);

        $images = $ad->photos()->get();


        if($ad === null){

            Session::flash('info', 'Не постои оглас со тоа ИД');

            return redirect()->back();
        }

        $last_four_ads = Ad::orderBy('created_at','desc')->take(4)->get();

        return view('singlead')
                            ->with('ad', $ad)
                            ->with('last_four_ads', $last_four_ads)
                            ->with('locations', Location::all())
                            ->with('statuses', Status::all())
                            ->with('types', Type::all())
                            ->with('images', $images);
    }

    public function deleteAdImage(Request $request){
        if(File::exists($request->url)) {
                File::delete($request->url);
        }
        $photo = AdPhoto::find($request->id);
        if($photo->delete()){
            return 1;
        }
        return 0;
    }
}
