<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Tag;

use Session;

class TagsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.tags.index')->with('tags', Tag::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tag = new Tag;

        $this->validate($request, [
            'tag' => 'required'
        ]);

        $tag->tag = $request->tag;
        

        if(Tag::whereSlug(str_slug($request->tag))->exists()){

            Session::flash('error', 'Веќе постои овој таг');
            
            return redirect()->back();
        }

        $tag->slug = str_slug($request->tag);
        $tag->save();

        Session::flash('success', 'Успешно креиравте таг');

        return redirect()->route('tags');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tag = Tag::find($id);

        return view('admin.tags.edit')->with('tag', $tag);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'tag' => 'required'
        ]);

        $tag = Tag::find($id);

        $tag->tag = $request->tag;

        if(Tag::whereSlug(str_slug($request->tag))->exists()){

            Session::flash('error', 'Веќе постои овој таг');
            
            return redirect()->back();
        }
        
        $tag->slug = str_slug($request->tag);
        $tag->save();

        Session::flash('success', 'Успешно променивте таг');

        return redirect()->route('tags');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {       

        $tag = Tag::find($id);

        // dd($tag);

        // dd($tag->posts->count()); 

        if($tag->posts->count()>=1){

            Session::flash('error', 'Не можете да го избришете овој таг');
            
            return redirect()->back();
        }

        $tag->delete();

        Session::flash('success', 'Успешно избришавте таг');

        return redirect()->route('tags'); 
    }
}