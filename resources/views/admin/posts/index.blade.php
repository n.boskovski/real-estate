@extends('layouts.app')

@section('content')	

	<table class="table table-hover">



		<tbody>

			@if($posts->count() == 0 )

			<thead>
				
				<th class="text-center">Се уште немате објавено содржина</th>

			</thead>
			
			@else

			<thead>
		
				<th>ИД</th>
				<th>Слика</th>
				<th>Наслов</th>
				<th>Промени</th>
				<th>Избриши</th>

			</thead>

			<tbody>


			
				@foreach($posts as $post)

					<tr>

						<td>							
							
							{{$post->id}}					

						</td>
						<td>							
							
							<img src="{{$post->featured}}" alt="{{$post->title}}" width="70px" height="40px">						

						</td>
						<td>							
							{{$post->title}}

						</td>		
						<td>							
							<a href="{{route('post.edit', ['id' => $post->id])}}" class="btn btn-sm btn-info">Промени</a>
						</td>
						<td>					
							<a href="{{route('post.destroy', ['id' => $post->id])}}" class="btn btn-sm btn-danger" onclick="return confirm('Дали сте сигурни дека сакате да го избришете огласот - {{$post->title}}')">Избриши</a>
						</td>
					</tr>

				@endforeach

			@endif

		</tbody>

	</table>

@stop


