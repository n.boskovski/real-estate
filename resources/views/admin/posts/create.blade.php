@extends('layouts.app')

@section('content')

    @include('admin.includes.errors')

    <div class="card">
        <div class="card-header">
            Креирај нова објава
        </div>

        <div class="card-body">
            <form action="{{route('post.store')}}" method="POST" enctype="multipart/form-data">

                <div class="form-group">

                    <label for="title">Наслов:</label>

                    <input type="text" name="title" class="form-control">

                </div>

                <div class="form-group">

                    <label for="featured">Слика:</label>

                    <input type="file" name="featured" class="form-control">

                </div>

                <div class="form-group">

                    <label for="content">Текст:</label>

                    <textarea class="form-control" name="content" cols="5" rows="10"></textarea>

                </div>

                @if($tags->count()>0)

                    <div class="form-group">

                        
                        <label for="title">Одбери таг:</label>

                            @foreach($tags as $tag)

                                <div class="checkbox">

                                    <label><input type="checkbox" name="tags[]" value="{{$tag->id}}">{{$tag->tag}} </label>

                                </div>
                                
                            @endforeach 

                    </div>

                @endif

                <div class="form-group">

                    <div class="text-center">

                        <button class="btn btn-success" type="submit">
                            Зачувај
                        </button>

                    </div>

                </div>               
                {{csrf_field()}}
            </form>        
        </div>
    </div>


@stop

