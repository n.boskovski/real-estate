@extends('layouts.app')

@section('content')

    @include('admin.includes.errors')

    <div class="card">
        <div class="card-header">
            Промени таг: {{$tag->tag}}
        </div>

        <div class="card-body">
            <form action="{{route('tag.update', ['id' => $tag->id])}}" method="POST">


				<div class="form-group">

					<label for="tag">Таг</label>

					<input type="text" name="tag" class="form-control" value="{{$tag->tag}}">

				</div>

				
				<div class="form-group">

					<div class="text-center">
						
						<button class="btn btn-success" type="submit">Зачувај</button>

					</div>

				</div>

				@include('admin.includes.errors')
				
				{{csrf_field()}}

			</form>
        </div>
    </div>


@stop

