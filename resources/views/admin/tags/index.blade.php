@extends('layouts.app')

@section('content')	

	<table class="table table-hover">



		<tbody>

			@if($tags->count() == 0 )

			<thead>
				
				<th class="text-center">Се уште немате внесено тагови</th>

			</thead>
			
			@else

			<thead>
		
				<th>Таг</th>
				<th>Промени</th>
				<th>Избриши</th>

			</thead>

			<tbody>


			
				@foreach($tags as $tag)

					<tr>

						<td>
							
							{{$tag->tag}}

						</td>
		
						<td>
							
							<a href="{{route('tag.edit',['id' => $tag->id])}}" class="btn btn-sm btn-info">Промени</a>

						</td>

						<td>
							
							<a href="{{route('tag.destroy',['id' => $tag->id])}}" class="btn btn-sm btn-danger" onclick="return confirm('Дали сте сигурни дека сакате да го избришете тагот - {{$tag->tag}}')">Избриши</a>

						</td>

					</tr>

				@endforeach

			@endif

		</tbody>

	</table>

@stop


