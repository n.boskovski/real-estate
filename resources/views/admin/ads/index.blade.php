@extends('layouts.app')

@section('content')	

	<table class="table table-hover">


		<tbody>

			@if($ads->count() == 0 )

				<thead>
					
					<th class="text-center">Се уште немате внесено оглас</th>

				</thead>
			
			@else

			<thead>
		
				<th>ИД</th>
				<th>Слика</th>
				<th>Наслов</th>
				<th>Промени</th>
				<th>Избриши</th>

			</thead>

			<tbody>


			
				@foreach($ads as $ad)

					<tr>

						<td>							
							
							{{$ad->id}}						

						</td>
						<td>							
							
							<img src="{{$ad->featured}}" alt="{{$ad->title}}" width="70px" height="40px">						

						</td>
						<td>							
							{{$ad->title}}

						</td>		
						<td>							
							<a href="{{route('ad.edit', ['id' => $ad->id])}}" class="btn btn-sm btn-info">Промени</a>
						</td>
						<td>					
							<a href="{{route('ad.destroy', ['id' => $ad->id])}}" class="btn btn-sm btn-danger" onclick="return confirm('Дали сте сигурни дека сакате да го избришете огласот - {{$ad->title}}')">Избриши</a>
						</td>
					</tr>

				@endforeach

			@endif

		</tbody>

	</table>

@stop


