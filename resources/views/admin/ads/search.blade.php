

@extends('layouts.app')

@section('content')

    @include('admin.includes.errors')

    <div class="card">

        <div class="card-header">
            Пребарај оглас по ИД
        </div>

        <div class="card-body">
            
            <form action="{{route('ad.result')}}" method="GET">

	            <div class="form-group">

	                <label for="id">ИД:</label>

	                <input type="text" name="id" class="form-control" required>

	            </div>

	            <div class="form-group">

	                    <div class="text-center">

	                        <button class="btn btn-success" type="submit">
	                            Барај
	                        </button>

	                    </div>

	                </div>  

	            {{csrf_field()}}

			</form>
	                
	    </div>

	</div>

@stop
