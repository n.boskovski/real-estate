@extends('layouts.app')

@section('content')

    @include('admin.includes.errors')

    <div class="card">
        <div class="card-header">
            Промени оглас: {{$ad->title}}
        </div>

        <div class="card-body">
            <form action="{{route('ad.update', ['id' => $ad->id])}}" method="POST" enctype="multipart/form-data">

                <div class="form-group">

                    <label for="title">Наслов:</label>

                    <input type="text" name="title" class="form-control"> 

                </div>

                <div class="form-group">

                    <label for="location">Избери тип:</label>

                    <select name="type_id" class="form-control">
                        
                        @foreach($types as $type)

                            <option value="{{$type->id}}"

                                @if($ad->type_id == $type->id)

                                selected

                                @endif 

                            >{{$type->name}}</option>

                        @endforeach

                    </select>

                </div>

                <div class="form-group">

                    <label for="location">Избери локација:</label>

                    <select name="location_id" class="form-control">
                        
                        @foreach($locations as $location)

                            <option value="{{$location->id}}"

                                @if($ad->location_id == $location->id)

                                selected

                                @endif 

                            >{{$location->name}}</option>

                        @endforeach

                    </select>

                </div>

                <div class="form-group">

                    <label for="status">Избери статус:</label>

                    <select name="status_id" class="form-control">
                        
                        @foreach($statuses as $status)

                            <option value="{{$status->id}}"

                                @if($ad->id == $status->id)

                                selected

                                @endif 

                            >{{$status->name}}</option>

                        @endforeach

                    </select>

                </div>

                <div class="form-group">

                    <label for="featured">Главна слика:</label>

                    <input type="file" name="featured" class="form-control">

                </div>

                <div class="form-group photos">

{{--                     <label for="featured">Додај слики:</label> --}}

                    <div id="add-photo" class="btn btn-info">Додај слика</div>

                </div>

                

                    @foreach($ad->photos as $photo)
                    <div>
                        <img src="{{asset($photo->url)}}"  width="90px" height="60px">                     
                        <span data-url="{{$photo->url}}" data-photo-id="{{$photo->id}}" class="delete-picture">избриши</span>
                         </div>
                    @endforeach               


                <div class="form-group">

                    <label for="content">Опис:</label>

                    <textarea class="form-control" name="content" id="content" cols="5" rows="10">{{$ad->content}}</textarea>

                </div>

                <div class="form-group">

                    <label for="price">Цена:</label>

                    <input type="text" name="price" value="{{$ad->price}}">

                </div>

                <div class="form-group">

                    <label for="area">Квадратура:</label>

                    <input type="text" name="area" value="{{$ad->area}}">

                </div>

                <div class="form-group">

                    <label for="bedrooms">Спални соби: </label>

                    <input type="number" name="bedrooms" min="1" max="5" value="{{$ad->bedrooms}}">

                    <label for="bathrooms">Тоалети :</label>

                    <input type="number" name="bathrooms" min="1" max="5" value="{{$ad->bathrooms}}">

                </div>

                <div class="form-group">

                    <div class="text-center">

                        <button class="btn btn-success" type="submit">
                            Зачувај
                        </button>

                    </div>

                </div>               
                {{csrf_field()}}
            </form>        
        </div>
    </div>


@endsection
