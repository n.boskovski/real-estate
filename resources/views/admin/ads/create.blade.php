@extends('layouts.app')

@section('content')

    @include('admin.includes.errors')

    <div class="card">
        <div class="card-header">
            Креирај нов оглас
        </div>

        <div class="card-body">
            <form action="{{route('ad.store')}}" method="POST" enctype="multipart/form-data">

                <div class="form-group">

                    <label for="title">Наслов:</label>

                    <input type="text" name="title" class="form-control">

                </div>

                <div class="form-group">

                    <label for="type">Избери тип:</label>

                    <select name="type_id" class="form-control">
                        
                        @foreach($types as $type)

                        <option value="{{$type->id}}">{{$type->name}}</option>

                        @endforeach

                    </select>

                </div>

                <div class="form-group">

                    <label for="location">Избери локација:</label>

                    <select name="location_id" class="form-control">
                        
                        @foreach($locations as $location)

                        <option value="{{$location->id}}">{{$location->name}}</option>

                        @endforeach

                    </select>

                </div>

                <div class="form-group">

                    <label for="status">Избери статус:</label>

                    <select name="status_id" class="form-control">
                        
                        @foreach($statuses as $status)

                        <option value="{{$status->id}}">{{$status->name}}</option>

                        @endforeach

                    </select>

                </div>


                <div class="form-group">

                    <label for="featured">Главна слика:</label>

                    <input type="file" name="featured" class="form-control">

                </div>

                <div class="form-group photos">

{{--                     <label for="featured">Додај слики:</label> --}}

                    <div id="add-photo" class="btn btn-info">Додај слика</div>

                </div>



                <div class="form-group">

                    <label for="content">Опис:</label>

                    <textarea class="form-control" name="content" cols="5" rows="5"></textarea>

                </div>

                <div class="form-group">

                    <label for="price">Цена:</label>

                    <input type="text" name="price">

                </div>

                <div class="form-group">

                    <label for="area">Квадратура:</label>

                    <input type="text" name="area">

                </div>

                <div class="form-group">

                    <label for="bedrooms">Спални соби: </label>

                    <input type="number" name="bedrooms" min="1" max="5">

                    <label for="bathrooms">Тоалети :</label>

                    <input type="number" name="bathrooms" min="1" max="5">

                </div>

                <div class="form-group">

                    <div class="text-center">

                        <button class="btn btn-success" type="submit">
                            Зачувај
                        </button>

                    </div>

                </div>               
                {{csrf_field()}}
            </form>        
        </div>
    </div>


@stop
