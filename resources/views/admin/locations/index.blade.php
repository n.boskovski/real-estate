@extends('layouts.app')

@section('content')	

	<table class="table table-hover">



		<tbody>

			@if($locations->count() == 0 )

			<thead>
				
				<th class="text-center">Се уште немате внесено локации</th>

			</thead>
			
			@else

			<thead>
		
				<th>Локација</th>
				<th>Промени</th>
				<th>Избриши</th>

			</thead>

			<tbody>


			
				@foreach($locations as $location)

					<tr>

						<td>
							
							{{$location->name}}

						</td>
		
						<td>
							
							<a href="{{route('location.edit',['id' => $location->id])}}" class="btn btn-sm btn-info">Промени</a>

						</td>

						<td>
							
							<a href="{{route('location.destroy',['id' => $location->id])}}" class="btn btn-sm btn-danger" onclick="return confirm('Дали сте сигурни дека сакате да ја избришете локацијата - {{$location->name}}')">Избриши</a>

						</td>

					</tr>

				@endforeach

			@endif

		</tbody>

	</table>

@stop


