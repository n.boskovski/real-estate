@extends('layouts.app')

@section('content')

    @include('admin.includes.errors')

    <div class="card">
        <div class="card-header">
            Додади локација
        </div>

        <div class="card-body">
            <form action="{{route('location.store')}}" method="POST">

                <div class="form-group">

                    <label for="title">Локација:</label>

                    <input type="text" name="location" class="form-control">

                </div>

                <div class="form-group">

                    <div class="text-center">

                        <button class="btn btn-success" type="submit">
                            Зачувај
                        </button>

                    </div>

                </div>               
                {{csrf_field()}}
            </form>        
        </div>
    </div>


@endsection
