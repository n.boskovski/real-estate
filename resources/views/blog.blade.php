<!DOCTYPE html>
<html>
<head>

	<title>Блог | Маком Недвижности</title>

	@include('includes.head')

</head>
<body>

	<!-- NAVBAR -->

    @include('includes.navbar')

	<!-- BLOG HEADER -->

    <div class="container-fluid">
		<div class="row blog-header">
			<div class= "blog-header-overlay">
				<div class="container">
					<h1 class="header-text">Блог</h1>
				</div>
			</div>
		</div>
	</div>

	<!-- ALL BLOG POSTS -->

	<div class="blog-content-area">
	    <div class="container">
	        <div class="row">
	        	@foreach($blog_posts as $post)
					<div class="col-md-4 col-sm-6">
		                <div class="blog">
		                    <div class="blog-photo blog-photo-cover">
		                        <img src="{{$post->featured}}">
		                    </div>
		                    <div class="blog-detail">
		                        <h3>
		                            <a href="{{route('single.post', ['slug' => $post->slug])}}">{{$post->title}}</a>
		                        </h3>
		                        <p>
		                        	{{ substr(strip_tags($post->content), 0, 140)}} {{strlen($post->content) > 140 ? "..." : ""}}
		                        </p>
		                        <div class="blog-read-more">
		                        	<a href="{{route('single.post', ['slug' => $post->slug])}}">Прочитај повеќе</a>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        @endforeach
	        </div>
        	<div class="row">
		        <div class="col-md-4 col-md-offset-4 text-center">
			    	{!!$blog_posts->links()!!}
			    </div>
			</div>
	    </div>
	</div>

	<!-- FOOTER -->

	@include('includes.footer')

</body>
</html>		

