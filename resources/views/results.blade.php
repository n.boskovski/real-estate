<!DOCTYPE html>
<html>
<head>

	<title>Резултати | Маком Недвижности</title>
	
	<!-- HEAD -->
	@include('includes.head')

</head>
<body>

	<!-- NAVBAR -->

	@include('includes.navbar')

	<!-- RESULTS HEADER -->

    <div class="container-fluid">
		<div class="row results-header">
			<div class= "results-header-overlay">
				<div class="container">
					<h1 class="header-text">Резултати</h1>
				</div>
			</div>
		</div>
	</div>

	
	<!-- LISTING RESULTS -->

	<div class="container" >
		<div class="row">
			@if(count($ads)>0)
				@foreach ($ads as $ad)
				<div class="col-md-4 col-sm-6 list-item list-item-cover">
					<img src="{{$ad->featured}}" alt="">
					<div class="list-text">
						<div class="text-center list-title">
                            <h1>
                                <a href="{{route('single.ad', ['slug' => $ad->slug])}}">{{$ad->title}}</a>
                            </h1>
							<p><i class="fas fa-map-marker-alt"></i> {{$ad->location->name}}</p>
						</div>
						<div class="list-info">
	                        <p><i class="fa fa-th-large"></i> {{$ad->area}} квадрати</p>
	                        

	                        @if(!empty ($ad->bedrooms)) 

	                            @if($ad->bathrooms>1) 

	                                <p><i class="fa fa-bed"></i> {{$ad->bedrooms}} спални</p>                                

	                                @else

	                                <p><i class="fa fa-bed"></i> {{$ad->bedrooms}} спална</p>

	                            @endif                            

	                        @endif
	                        

	                        @if(!empty ($ad->bathrooms)) 

	                            @if($ad->bathrooms>1) 

	                                <p><i class="fa fa-bath"></i> {{$ad->bathrooms}} тоалети</p>                                

	                                @else

	                                <p><i class="fa fa-bath"></i> {{$ad->bathrooms}} тоалет</p>

	                            @endif                            

	                        @endif

	                    </div>
	                    <div class="list-price">
	                        <a href="{{route('single.ad', ['slug' => $ad->slug])}}"> {{$ad->price}} &euro;</a>
	                    </div>       				
					</div>
				</div>
				@endforeach
            @else

                <h2 class="results-title text-center">Нема резултати за вашето пребарување, обидете се повторно</h1>

            @endif
		</div>
{{--     	<div class="row">
	        <div class="col-md-4 col-md-offset-4 text-center">
		    	{!!$ads->links()!!}
		    </div>
		</div> --}}
	</div>

	<!-- FOOTER -->

	@include('includes.footer')

</body>
</html>		
