<!DOCTYPE html>
<html>
<head>

    <title>Маком Недвижности</title>

    @include('includes.head')

    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Маком Недвижности"/>    
    <meta property="og:description" content="Купување и продавање недвижен имот без агенциска провизија од страна на купувачот."/>
    <meta property="og:image" content="{{asset('app/images/realestate2.jpg')}}"/>
    <meta property="og:url" content="https://makom.mk"/>

</head>
<body>

    <!-- NAVBAR -->

    @include('includes.navbar')

    <!-- HEADER -->

    <div class="container-fluid">
        <div class="row header">
            <div class="header-overlay ">
                <div class="container">
                    <h1 class="header-text">Пронајдете го вашиот идеален дом</h1>
                    <div class="filter" style="font-size: 0">                   
                        <form method="POST" action="{{route('results')}}"> 
                            {{ csrf_field() }}   
                            <select name="status" class="filter-form">
                                <option value="">Статус</option>                                
                                    @foreach($statuses as $status)
                                    
                                    <option value="{{$status->id}}">{{$status->name}}</option>

                                @endforeach
                            </select>
                            <select name="type" class="filter-form">
                                <option value="">Тип</option>
                                
                                    @foreach($types as $type)

                                    
                                    <option value="{{$type->id}}">{{$type->name}}</option>

                                @endforeach
                            </select>
                            <select name="location" class="filter-form">

                                <option value="">Локација</option>
                                
                                    @foreach($locations as $location)

                                    
                                    <option value="{{$location->id}}">{{$location->name}}</option>

                                @endforeach

                            </select>
                            <select name="bedrooms" class="filter-form">
                                <option value="">Спални</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                            <button class="filter-search filter-form"><i class="fas fa-search"></i> Барај</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- LAST 6 LISTINGS -->

    <div class="container">
        <div class="list-section-title text-center">
            <h2>Последни огласи</h2>
        </div>
        <div class="row">

            @foreach($last_six_lists as $list)
                <div class="col-md-4 col-sm-6 list-item list-item-cover">
                    <img src="{{$list->featured}}" alt="">
                    <div class="list-text">
                        <div class="text-center list-title">
                            <h1>
                                <a href="{{route('single.ad', ['slug' => $list->slug])}}">{{$list->title}}</a>
                            </h1>
                            <p><i class="fas fa-map-marker-alt"></i> {{$list->location->name}}</p>
                        </div>
                        <div class="list-info">
                            <p><i class="fa fa-th-large"></i> {{$list->area}} квадрати</p>
                            

                            @if(!empty ($list->bedrooms)) 

                                @if($list->bathrooms>1) 

                                    <p><i class="fa fa-bed"></i> {{$list->bedrooms}} спални</p>                                

                                    @else

                                    <p><i class="fa fa-bed"></i> {{$list->bedrooms}} спална</p>

                                @endif                            

                            @endif
                            

                            @if(!empty ($list->bathrooms)) 

                                @if($list->bathrooms>1) 

                                    <p><i class="fa fa-bath"></i> {{$list->bathrooms}} тоалети</p>                                

                                    @else

                                    <p><i class="fa fa-bath"></i> {{$list->bathrooms}} тоалет</p>

                                @endif                            

                            @endif

                        </div>
                        <div class="list-price">
                            <a href="{{route('single.ad', ['slug' => $list->slug])}}"> {{$list->price}} &euro;</a>
                        </div>                  
                    </div>
                </div>
            @endforeach

            <div class="load-more">                
                <a href="{{route('allads')}}">СИТЕ ОГЛАСИ</a>
            </div>
        </div>
    </div>

    <!-- SERVICES -->

    <div class="services content-area">
        <div class="container">
            <div class="main-title text-center">
                <h2>Затоа што домот е чувство на топлина и сигурност</h2>
                <p></p>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="service-info">
                        <div class="icon">
                            <i class="fas fa-building"></i>
                        </div>
                        <h3>Градежна компанија</h3>
                        <p>Инвеститор и изведувач на градби од трета, четврта и петта категорија</p>
                    </div>
                </div>
                <div class=" col-lg-4 col-md-4 col-sm-6 d-none d-xl-block d-lg-block">
                    <div class="service-info">
                        <div class="icon">
                            <i class="fas fa-map-marked-alt"></i>
                        </div>
                        <h3>Агенција за недвижности</h3>
                        <p>Посредување при купување и продавање недвижен имот без агенциска провизија од страна на купувачот</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="service-info">
                        <div class="icon">
                            <i class="fas fa-user"></i>
                        </div>
                        <h3>Консалтинг</h3>
                        <p>Советување при купопродажба на недвижен имот, при кредитирање и правна помош</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--BLOG-->

    <div class="container">
        <div class="blog-section-title text-center">
            <h2>Последни блог постови</h2>
        </div>
        <div class="row">
            @foreach($last_three_posts as $post)
                <div class="col-md-4 col-sm-6">
                    <div class="blog">
                        <div class="blog-photo blog-photo-cover">
                            <img src="{{$post->featured}}">
                        </div>
                        <div class="blog-detail">
                            <h3>                                
                                <a href="{{route('single.post', ['slug' => $post->slug])}}">{{$post->title}}</a>
                            </h3>
                            <p>
                                {{ substr(strip_tags($post->content), 0, 140)}} {{strlen($post->content) > 140 ? "..." : ""}}
                            </p>
                            <div class="blog-read-more">
                                <a href="{{route('single.post', ['slug' => $post->slug])}}">Прочитај повеќе</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <!-- FOOTER -->

    @include('includes.footer')

</body>
</html>     
