
    
    <meta charset="utf-8">  
    <meta name="viewport" content="width=device-width, initial-scale=1.0">  
    <meta name="keywords" content="nedviznosti, nedviznosti skopje, gradeznistvo, agencija za nedviznosti, real estate skopje, agencija za nedviznosti skopje, агенција за недвижности, маком недвижности, маком, агенција за недвижности скопје, недвижности, недвижности скопје, градежништво, купување стан, продавање стан">
    <meta name="description" content="Makom.mk - Посредување при купување/продавање на недвижности и гаранција за чиста документација на истите. /">
    <meta name="author" content="Ненад Бошковски">



    <link rel="icon" type="image/ico" href="{{asset('app/images/logo.png')}}" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5ca79a39f5265ba6"></script>

    <link rel="stylesheet" type="text/css" href="{{asset('app/css/style.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('app/css/slick.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app/css/slick-theme.css')}}">

    <meta property="fb:app_id" content="864746630534317"/>
    <meta property="og:site_name" content="Маком Недвижности"/>


    


