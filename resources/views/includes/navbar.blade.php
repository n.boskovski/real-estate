    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container" class="">
            <div class="navbar-header">
                <button type="button"class="navbar-toggle"data-toggle="collapse"
                data-target="#example-navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                <div class="logo-responsive">
                    <a href="{{route('index')}}"><img src="{{asset('app/images/logo.png')}}" alt=""></a>
                </div>
                <div class="logo">
                    <a href="{{route('index')}}"><img src="{{asset('app/images/logo-white.png')}}" alt=""></a>
                </div>
            </div>
            <div class="collapse navbar-collapse" id="example-navbar-collapse">       
                <ul class="nav navbar-nav navbar-right">
                    <ul class="nav navbar-nav">
                        <li><a href="{{route('index')}}">Почетна</a></li>
                        <li><a href="{{route('allads')}}">Сите огласи</a></li>
                        <li><a href="{{route('blog')}}">Блог</a></li>
                        <li><a href="{{route('about')}}">За Нас</a></li> 
                        <li><a href="{{route('contact')}}">Контакт</a></li>
                        {{-- <li><a href="#"><span class="glyphicon glyphicon-search"></span></a></li> --}}
                    </ul>  
                </ul>
            </div>
        </div>
    </nav>