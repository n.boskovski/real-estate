    <footer class="footer">
        <div class="container footer-inner">
            <div class="row">
                <div class="col-md-4 col-md-offset-2 col-sm-4">
                    <div class="footer-item">
                        <div class="text-center">
                            <a href="index.html"> <img src="{{asset('app/images/logo-white.png')}}" alt="logo" class="f-logo"> </a>
                        </div>
                        <div class="text ">
                            <p>Посредуваме при купување / продавање на недвижности и гарантираме за чиста документација на истите. Бидете сигурни за инвестирање во иднината, најдете дом во кој ќе се чувствувате безбедно и удобно цел живот.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4">
                    <div class="footer-item">
                        <h4>Контакт</h4>
                        <div class="f-border"></div>
                        <ul class="contact-info">
                            <li>
                                <i class="fas fa-map-marker-alt"></i><span>Виа Игнатиа 4, 1000 Скопје, Македонија</span>
                            </li>
                            <li class="contact-info-meil">
                                <i class="far fa-envelope"></i><span><a href="">info@мakom.mk</a></span>
                            </li>
                            <li>
                                <i class="fas fa-phone"></i><span>+389 72 224 164</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4">
                    <div class="footer-item">
                        <h4>
                            Линкови
                        </h4>
                        <div class="f-border"></div>
                        <ul class="links">
                            <li>
                                <a href="{{route('index')}}">Почетна</a>
                            </li>
                            <li>
                                <a href="{{route('allads')}}">Сите огласи</a>
                            </li>
                            <li>
                                <a href="{{route('about')}}">За Нас</a>
                            </li>
                            <li>
                                <a href="{{route('blog')}}">Блог</a>
                            </li>
                            <li>
                                <a href="{{route('contact')}}">Контакт</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div>
                <div class="social-list">
                    <a href="https://www.facebook.com/makomnedviznosti/" class="facebook" target="blank"><i class="fab fa-facebook-square"></i></a>
                    <a href="https://www.instagram.com/makomnedviznosti/?hl=en" target="blank" class="twitter"><i class="fab fa-instagram"></i></a>
                </div>
            </div>
            <div class="row sub-footer">
                <div>
                    <p>© 2019 Маком Недвижности</p>
                </div>                
            </div>
        </div>
    </footer>