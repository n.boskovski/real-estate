<!DOCTYPE html>
<html>
<head>
	
	<title> {{$ad->title}} | Маком Недвижности</title>
	<!-- HEAD -->
	@include('includes.head')

	<meta property="og:type" content="article"/>
    <meta property="og:title" content="{{$ad->title}} | Маком Недвижности"/>    
    <meta property="og:description" content="{{strip_tags($ad->content)}} "/>
    <meta property="og:image" content="{{$ad->featured}}"/>
    <meta property="og:url" content="https://makom.mk/ad/{{$ad->slug}}"/>

</head>
<body>

	<!-- NAVBAR -->

	@include('includes.navbar')

	<!-- SINGLE LIST HEADER -->

    <div class="container-fluid">
		<div class="row single-list-header">
			<div class= "single-list-header-overlay">
				<div class="container">
					<h1 class="header-text">Детали за огласот</h1>
				</div>
			</div>
		</div>
	</div>

	<!-- SINGLE LIST ITEM -->

	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-12 single-list-item single-list-item-cover">
				<div class="imageSlider">
					<div><img src="{{$ad->featured}}" alt=""></div>

					@foreach($images as $image)

						<div><img src="{{asset($image->url)}}" alt=""></div>

					@endforeach
				</div>	
				<div class="single-list-heading">
					<div class="sinle-list-title">
						<h1>{{$ad->title}}</h1>
					</div>
					<div class="single-list-price">
						<span>{{$ad->price}}&euro;</span>
					</div>
	            </div>
            	<span class="single-list-id">
            		# {{$ad->id}}
            	</span>
            	<span class="single-list-location">
            		<i class="fas fa-map-marker-alt"></i> {{$ad->location->name}}
            	</span>
				<p>{!!$ad->content!!}</p>
				<div class="single-list-details">
					<p><i class="fa fa-th-large"></i> {{$ad->area}} квадрати</p>
					@if(!empty ($ad->bedrooms)) 

                        @if($ad->bathrooms>1) 

                            <p><i class="fa fa-bed"></i> {{$ad->bedrooms}} спални</p>                                

                            @else

                            <p><i class="fa fa-bed"></i> {{$ad->bedrooms}} спална</p>

                        @endif                            

                    @endif
                    

                    @if(!empty ($ad->bathrooms)) 

                        @if($ad->bathrooms>1) 

                            <p><i class="fa fa-bath"></i> {{$ad->bathrooms}} тоалети</p>                                

                            @else

                            <p><i class="fa fa-bath"></i> {{$ad->bathrooms}} тоалет</p>

                        @endif                            

                    @endif
				</div>
				<div class="single-list-price-responsive"> {{$ad->price}}&euro;</div>
	            <!-- Go to www.addthis.com/dashboard to customize your tools -->
    			<div class="add-this addthis_inline_share_toolbox"></div>			
			</div>

			<!-- SINGLE LIST FILTER -->

		   	<div class="col-md-4">
				<div style="font-size: 0">					
					<form method="POST" action="{{route('results')}}" class="s-listing-filter listing-filter">
						{{ csrf_field() }}
						<select name="status" class="s-listing-filter-select listing-filter-select">
							<option value="">Статус</option>                                
		                        @foreach($statuses as $status)
		                        
			                        <option value="{{$status->id}}">{{$status->name}}</option>

			                    @endforeach
						</select>
						<select name="type" class="s-listing-filter-select listing-filter-select">
							<option value="">Тип</option>
                    
		                        @foreach($types as $type)
		                        
			                        <option value="{{$type->id}}">{{$type->name}}</option>

			                    @endforeach
						</select>
						<select name="location" class="s-listing-filter-select listing-filter-select">
							<option value="">Локација</option>
		                                
		                        @foreach($locations as $location)
		                        
			                        <option value="{{$location->id}}">{{$location->name}}</option>

			                    @endforeach
						</select>
						<select name="bedrooms" class="s-listing-filter-select listing-filter-select">
							<option value="">Спални</option>
			                @for($i = 1; $i <= 5; $i++)

		                		<option value="{{$i}}">{{$i}}</option>
			               	 
			                @endfor
						</select>
						<select name="bathrooms" class="s-listing-filter-select listing-filter-select">
							<option value="">Тоалети</option>
			                @for($i = 1; $i <= 3; $i++)

		                		<option value="{{$i}}">{{$i}}</option>
			               	 
			                @endfor
						</select>
						<button class="s-listing-filter-select listing-filter-select listing-filter-search"><i class="fas fa-search"></i> Барај</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<!-- SIMILAR LISTINGS -->

	<div class="container">
		<div class="similar-list-section-title text-left">
			<h3>Последни огласи</h3>
		</div>
		<div class="row">
			@foreach($last_four_ads as $ad)
				<div class="col-lg-3 col-md-3 col-sm-6 list-item similar-list-item-cover">
					<img src="{{$ad->featured}}" alt="">
					<div class="list-text">
						<div class="text-center list-title s-listings-list-title">
                            <h1>
                                <a href="{{route('single.ad', ['slug' => $ad->slug])}}">{{$ad->title}}</a>
                            </h1>
							<p><i class="fas fa-map-marker-alt"></i> {{$ad->location->name}}</p>
						</div>
						<div class="list-info">
							<p><i class="fa fa-th-large"></i> {{$ad->area}}</p>
							@if(!empty ($ad->bedrooms)) 

                                @if($ad->bathrooms>1) 

                                    <p><i class="fa fa-bed"></i> {{$ad->bedrooms}} спални</p>                                

                                    @else

                                    <p><i class="fa fa-bed"></i> {{$ad->bedrooms}} спална</p>

                                @endif                            

                            @endif
                            

                            @if(!empty ($ad->bathrooms)) 

                                @if($ad->bathrooms>1) 

                                    <p><i class="fa fa-bath"></i> {{$ad->bathrooms}} тоалети</p>                                

                                    @else

                                    <p><i class="fa fa-bath"></i> {{$ad->bathrooms}} тоалет</p>

                                @endif                            

                            @endif
						</div>
						<div class="list-price">
							<a href="#">{{$ad->price}} &euro;</a>
						</div>					
					</div>

				</div>
			@endforeach
		</div>
	</div>

	<!-- FOOTER -->

	@include('includes.footer')

	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5ca79a39f5265ba6"></script>


	<script type="text/javascript" src="{{asset('app/css/slick.js')}}"></script>
		<script type="text/javascript">
			
			$(document).ready(function(){

				$('.imageSlider').slick({

					dots: true

				})
			});

	</script>

</body>
</html>		
