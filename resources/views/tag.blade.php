<!DOCTYPE html>
<html>
<head>

	<title>Маком Недвижности</title>
	
	<!-- HEAD -->

	@include('includes.head')

</head>
<body>

	<!-- NAVBAR -->

	@include('includes.navbar')

	<!-- TAG HEADER -->

    <div class="container-fluid">
		<div class="row tag-header">
			<div class= "tag-header-overlay">
				<div class="container">
					<h1 class="header-text">Таг: {{$tag->tag}}</h1>
				</div>
			</div>
		</div>
	</div>

	
	<!-- LISTING RESULTS -->

	<div class="container" >
		<div class="row">
			@foreach ($tag->posts as $post)
				<div class="col-md-4 col-sm-6">
	                <div class="blog">
	                    <div class="blog-photo blog-photo-cover">
	                        <img src="{{$post->featured}}">
	                    </div>
	                    <div class="blog-detail">
	                        <h3>
	                            <a href="{{route('single.post', ['slug' => $post->slug])}}">{{$post->title}}</a>
	                        </h3>
	                        <p>
	                        	{{ substr(strip_tags($post->content), 0, 140)}} {{strlen($post->content) > 140 ? "..." : ""}}
	                        </p>
	                        <div class="blog-read-more">
	                        	<a href="{{route('single.post', ['slug' => $post->slug])}}">Прочитај повеќе</a>
	                        </div>
	                    </div>
	                </div>
	            </div>
			@endforeach
		</div>
{{--     	<div class="row">
	        <div class="col-md-4 col-md-offset-4 text-center">
		    	{!!$ads->links()!!}
		    </div>
		</div> --}}
	</div>

	<!-- FOOTER -->

	@include('includes.footer')

</body>
</html>		
