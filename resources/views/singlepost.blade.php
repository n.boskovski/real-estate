<!DOCTYPE html>
<html>
<head>

	<title> {{$post->title}} | Маком Недвижности</title>

    @include('includes.head')

   	<meta property="og:type" content="article"/>
    <meta property="og:title" content="{{$post->title}} | Маком Недвижности"/>    
    <meta property="og:description" content="{{strip_tags($post->content)}} "/>
    <meta property="og:image" content="{{$post->featured}}"/>
    <meta property="og:url" content="https://makom.mk/ad/{{$post->slug}}"/>

</head>
<body>

	<!-- NAVBAR -->

	@include('includes.navbar')

	<!-- SINGLE-BLOG-HEADER -->

    <div class="container-fluid">
		<div class="row single-blog-header">
			<div class= "single-blog-header-overlay">
				<div class="container">
					<h1 class="header-text">Блог пост</h1>
				</div>
			</div>
		</div>
	</div>


	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 single-list-item single-list-item-cover">
				<img src="{{$post->featured}}" alt="">
				<div class="single-list-heading">
                    <h1>{{$post->title}}</h1>
                </div>
				<p>{!!$post->content!!}</p>

				@if(count($post->tags)>0)
					<div class="row">
						<div class="col-md-12 blog-tags">
							<span>Тагови: </span>

								@foreach($post->tags as $tag)

		                            <a href="{{route('tag.single', ['slug' => $tag->slug])}}">{{$tag->tag}}</a>

		                        @endforeach

						</div>
					</div>
				@endif
	            <!-- Go to www.addthis.com/dashboard to customize your tools -->
        		<div class="add-this addthis_inline_share_toolbox"></div>
			</div>
		</div>
	</div>

	<!-- FOOTER -->

	@include('includes.footer')

	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5ca79a39f5265ba6"></script>	

</body>
</html>		
