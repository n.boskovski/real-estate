<!DOCTYPE html>
<html>
<head>

	<title>Контакт | Маком Недвижности</title>
		
	@include('includes.head')

</head>
<body>

	<!-- NAVBAR -->

	@include('includes.navbar')

	<!-- CONTACT HEADER  -->

    <div class="container-fluid">
		<div class="row contact-header">
			<div class= "contact-header-overlay">
				<div class="container">
					<h1 class="header-text">Контакт</h1>
				</div>
			</div>
		</div>
	</div>

	<!--  CONTACT -->

	<div class="contact">
	    <div class="container">
	        <div class="row contact-info-items">
	            <div class="col-md-3 col-sm-6 col-xs-12 contact-item">
	                <i class="fas fa-map-marker-alt"></i>
	                <p>Адреса</p>
	                <p>Виа Игнатиа 4, 1000 Скопје</p>
	            </div>
	            <div class="col-md-3 col-sm-6 col-xs-12 contact-item">
	                <i class="fas fa-phone"></i>
	                <p>Телефонски број</p>
	                <p>+389 72 224 164</p>
	            </div>
	            <div class="col-md-3 col-sm-6 col-xs-12 contact-item">
	                <i class="fas fa-envelope-open"></i>
	                <p>Е-маил</p>
	                <p>info@мakom.mk</p>
	            </div>
	            <div class="col-md-3 col-sm-6 col-xs-12 contact-item">
	                <i class="fas fa-globe"></i>
	                <p>Веб</p>
	                <p>www.makom.mk</p>
	            </div>
	        </div>
	        <div class="row">

	        	<div class="row">

	        		<div class="col-md-4 col-md-offset-4">

		        		@if (count($errors)>0)

		                <div class="alert alert-danger">                          
		                    <button type="button" class="close" data-dismiss="alert">x</button>
		                    <ul>
		                        @foreach($errors->all() as $error)
		                            <li>{{$error}}</li>
		                        @endforeach
		                    </ul>
		                </div>
		                    
		                @endif 

		                @if($message = Session::get('succes'))

		                	<div class="alert alert-success alert-block">                		
		                        <button type="button" class="close" data-dismiss="alert">x</button>
		                        <strong> {{$message}} </strong>			
		                	</div>

		                @endif

	        		</div>
	        	</div> 

	        	<div class="col-md-8 col-md-offset-2 contact-form">
	                <form action="{{route('send.meil')}}" method="POST">
	                	{{csrf_field()}}
	                    <div class="row">
	                        <div class="col-md-6 col-sm-12 col-xs-12">
	                            <div class="form-group name">
	                                <input required type="text" name="name" class="form-control" placeholder="Име">
	                            </div>
	                        </div>
	                        <div class="col-md-6 col-sm-12 col-xs-12">
	                            <div class="form-group email">
	                                <input required type="email" name="email" class="form-control" placeholder="Е-маил">
	                            </div>
	                        </div>
	                        <div class="col-md-6 col-sm-12 col-xs-12">
	                            <div class="form-group subject">
	                                <input required type="text" name="subject" class="form-control" placeholder="Предмет">
	                            </div>
	                        </div>
	                        <div class="col-md-6 col-sm-12 col-xs-12">
	                            <div class="form-group number">
	                                <input required type="text" name="number" class="form-control" placeholder="Број">
	                            </div>
	                        </div>
	                        <div class="col-md-12 col-sm-12 col-xs-12">
	                            <div class="form-group message">
	                                <textarea required class="form-control" name="message" placeholder="Порака"></textarea>
	                            </div>
	                        </div>
	                        <div class="col-md-12 col-sm-12 col-xs-12">
	                            <div class="send-btn text-center">
	                                <button type="submit" class="btn btn-border btn-md">Испрати порака</button>
	                            </div>
	                        </div>
	                    </div>
	                </form>
	            </div>	        	
	        </div>		
		</div>
	</div>
	
	<!-- FOOTER -->

	@include('includes.footer')

</body>
</html>		

