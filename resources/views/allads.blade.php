<!DOCTYPE html>
<html>

	<title>Сите огласи | Маком Недвижности</title>
	
	{{-- HEAD --}}

    @include('includes.head')

<body>

	<!-- NAVBAR -->

	@include('includes.navbar')

	<!-- ALL LISTING HEADER -->

    <div class="container-fluid">
		<div class="row listing-header">
			<div class= "listing-header-overlay">
				<div class="container">
					<h1 class="header-text">Oгласи</h1>
				</div>
			</div>
		</div>
	</div>    

	<!-- ALL LISTING FILTER -->

    <div class="container-fluid" style="border-top: 1px solid #D4D4D4">
		<div style="font-size: 0">					
			<form id="adds-filter" class="listing-filter" method="POST" action="{{route('resultsajax')}}">
				{{ csrf_field() }} 
				<select name="status" class="listing-filter-select">
					<option value="">Статус</option>                                
                        @foreach($statuses as $status)
                        
                        <option value="{{$status->id}}"


                            @if($sta == $status->id)

                            selected

                            @endif 


                        >{{$status->name}}</option>

                    @endforeach
				</select>
				<select name="type" class="listing-filter-select">
					<option value="">Тип</option>
                    
                        @foreach($types as $type)
                        
                        <option value="{{$type->id}}"


                            @if($typ == $type->id)

                            selected

                            @endif 


                        >{{$type->name}}</option>

                    @endforeach
				</select>
				<select name="location" class="listing-filter-select">
					<option value="">Локација</option>
                                
                        @foreach($locations as $location)

                        
                        <option value="{{$location->id}}"

	                        @if($loc == $location->id)

		                    selected

		                    @endif 

                        >{{$location->name}}</option>

                    @endforeach
				</select>
				<select name="bedrooms" class="listing-filter-select">
					<option value="">Спални</option>
		                @for($i = 1; $i <= 5; $i++)

	                		<option value="{{$i}}"

		                		@if($bed == $i)

			                    selected

			                    @endif

	                		>{{$i}}</option>
		               	 
		                @endfor
				</select>
				<select name="bathrooms" class="listing-filter-select">
					<option value="">Тоалети</option>
		                @for($i = 1; $i <= 3; $i++)

	                		<option value="{{$i}}"

		                		@if($bat == $i)

			                    selected

			                    @endif

	                		>{{$i}}</option>
		               	 
		                @endfor
				</select>
				<button class="listing-filter-select listing-filter-search"><i class="fas fa-search"></i> Барај</button>
			</form>
		</div>
	</div>

	<!-- ALL LISTINGS FROM FILTER -->

	<div class="container">
		<div class="row" id="remove-lists-row">

			@if($ads->isEmpty())

			    <h2 class='results-title text-center'>Нема резултати за вашето пребарување, обидете се повторно</h1>

			@else

			    @foreach ($ads as $ad)
					<div class="col-md-4 col-sm-6 list-item list-item-cover remove-lists">
						<img src="{{$ad->featured}}" alt="">
						<div class="list-text">
							<div class="text-center list-title">
		                        <h1>
		                            <a href="{{route('single.ad', ['slug' => $ad->slug])}}">{{$ad->title}}</a>
		                        </h1>
								<p><i class="fas fa-map-marker-alt"></i> {{$ad->location->name}}</p>
							</div>
							<div class="list-info">
		                        <p><i class="fa fa-th-large"></i> {{$ad->area}} квадрати</p>
		                        

		                        @if(!empty ($ad->bedrooms)) 

		                            @if($ad->bathrooms>1) 

		                                <p><i class="fa fa-bed"></i> {{$ad->bedrooms}} спални</p>                                

		                                @else

		                                <p><i class="fa fa-bed"></i> {{$ad->bedrooms}} спална</p>

		                            @endif                            

		                        @endif
		                        

		                        @if(!empty ($ad->bathrooms)) 

		                            @if($ad->bathrooms>1) 

		                                <p><i class="fa fa-bath"></i> {{$ad->bathrooms}} тоалети</p>                                

		                                @else

		                                <p><i class="fa fa-bath"></i> {{$ad->bathrooms}} тоалет</p>

		                            @endif                            

		                        @endif

		                    </div>
		                    <div class="list-price">
		                        <a href="{{route('single.ad', ['slug' => $ad->slug])}}"> {{$ad->price}} &euro;</a>
		                    </div>       				
						</div>
					</div>
				@endforeach
			@endif
		</div>
{{-- 		<div class="row">
	        <div class="col-md-4 col-md-offset-4 text-center">
		    	{!!$ads->links()!!}
		    </div>
		</div> --}}
	</div>

	<!-- FOOTER -->

	@include ('includes.footer')


	<script type="text/javascript" src="{{asset('app/js/main.js')}}"></script>

</body>
</html>		

