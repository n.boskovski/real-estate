<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Ad;


Route::get('/', [
	'uses' => 'FrontEndController@index',
	'as' => 'index'	
]);
Route::get('/blog', [
	'uses' => 'FrontEndController@blog',
	'as' => 'blog'	
]);
Route::get('/contact', [
	'uses' => 'FrontEndController@contact',
	'as' => 'contact'	
]);
Route::get('/about-us', [
	'uses' => 'FrontEndController@aboutUs',
	'as' => 'about'	
]);
Route::get('/allads', [
	'uses' => 'FrontEndController@allads',
	'as' => 'allads'	
]);
Route::get('/post/{slug}', [
	'uses' => 'FrontEndController@singlePost',
	'as' => 'single.post'	
]);
Route::get('/ad/{slug}', [
	'uses' => 'FrontEndController@singleAd',
	'as' => 'single.ad'	
]);
Route::get('/tag/{slug}', [
	'uses' => 'FrontEndController@tag',
	'as' => 'tag.single'
]);




Route::post('/sendmail', [
	'uses' => 'FrontEndController@sendMail',
	'as' => 'send.meil'	
]);





Route::post('/results', [
	'uses' => 'FrontEndController@results',
	'as' => 'results'	
]);

Route::post('/resultsajax', [
	'uses' => 'FrontEndController@resultsajax',
	'as' => 'resultsajax'	
]);
Route::get('/delete-ad-image', [
	'uses' => 'AdsController@deleteAdImage',
	'as' => 'delete.ad.image'	
]);



Auth::routes();

// Auth::routes(['register' => false]);

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function(){

	Route::get('/home', 'HomeController@index')->name('home');

	Route::get('/ads',[
		'uses' => 'AdsController@index',
		'as' => 'ads'
	]);
		Route::get('/ad/create',[
		'uses' => 'AdsController@create',
		'as' => 'ad.create'
	]);
	Route::post('/ad/store',[
		'uses' => 'AdsController@store',
		'as' => 'ad.store'
	]);
	Route::get('/ad/destroy/{id}',[
		'uses' => 'AdsController@destroy',
		'as' => 'ad.destroy'
	]);
	Route::get('/ad/edit/{id}',[
		'uses' => 'AdsController@edit',
		'as' => 'ad.edit'
	]);
	Route::post('/ad/update/{id}',[
		'uses' => 'AdsController@update',
		'as' => 'ad.update'
	]);
	Route::get('/ad/search',[
		'uses' => 'AdsController@search',
		'as' => 'ad.search'
	]);
	Route::get('/ad/result',[
		'uses' => 'AdsController@result',
		'as' => 'ad.result'
	]);
 	Route::get('/locations',[
		'uses' => 'LocationsController@index',
		'as' => 'locations'
	]);
 	Route::get('/location/create',[
		'uses' => 'LocationsController@create',
		'as' => 'location.create'
	]);
 	Route::get('/location/edit/{id}',[
		'uses' => 'LocationsController@edit',
		'as' => 'location.edit'
	]);
 	Route::get('/location/destroy/{id}',[
		'uses' => 'LocationsController@destroy',
		'as' => 'location.destroy'
	]);
	Route::post('/location/update/{id}',[
		'uses' => 'LocationsController@update',
		'as' => 'location.update'
	]);
 	Route::post('/location/store',[
		'uses' => 'LocationsController@store',
		'as' => 'location.store'
	]); 
 	Route::get('/post/create',[
		'uses' => 'PostsController@create',
		'as' => 'post.create'
	]);
  	Route::post('/post/store',[
		'uses' => 'PostsController@store',
		'as' => 'post.store'
	]);
  	Route::get('/posts',[
		'uses' => 'PostsController@index',
		'as' => 'posts'
	]);
   	Route::get('/post/edit/{id}',[
		'uses' => 'PostsController@edit',
		'as' => 'post.edit'
	]);
   	Route::post('/post/update/{id}',[
		'uses' => 'PostsController@update',
		'as' => 'post.update'
	]);
	Route::get('/post/destroy/{id}',[
		'uses' => 'PostsController@destroy',
		'as' => 'post.destroy'
	]);
	Route::get('/tag/create',[
		'uses' => 'TagsController@create',
		'as' => 'tag.create'
	]);
	Route::get('/tags',[
		'uses' => 'TagsController@index',
		'as' => 'tags'
	]);
	Route::post('/tag/store',[
		'uses' => 'TagsController@store',
		'as' => 'tag.store'
	]);
   	Route::get('/tag/edit/{id}',[
		'uses' => 'TagsController@edit',
		'as' => 'tag.edit'
	]);
   	Route::post('/tag/update/{id}',[
		'uses' => 'TagsController@update',
		'as' => 'tag.update'
	]);
	Route::get('/tag/destroy/{id}',[
		'uses' => 'TagsController@destroy',
		'as' => 'tag.destroy'
	]);
});