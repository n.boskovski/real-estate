$(document).ready(function(){

	$("#adds-filter").submit(function(e){

		e.preventDefault();

		$.ajax({
		  type: 'POST',
		  url: $("#adds-filter").attr("action"),
		  data: $("#adds-filter").serialize(), 
		  success: function(response) {

			$(".remove-lists").fadeOut(200).remove();

			$(".results-title").fadeOut(200).remove();


			if(response == ""){

				var noresults= "<h2 class='results-title text-center'>Нема резултати за вашето пребарување, обидете се повторно</h2>";

				$("#remove-lists-row").append(noresults);

			}
			else {

		        $.each(response, function(key,val) {

		        	$("#remove-lists-row").append(val);

		        	$(".list-item").delay(205).fadeIn(300);

		            
		        });

			}

		  },


		});

	})

	// UPLOADING MORE PHOTOS 

	$("#add-photo").click(function(e){

		var photo = '<input type="file" accept="image/*" name="photos[]" class="form-control">';

		$(".photos").append(photo);

	})


	$(".delete-picture").click(function(e){

		var url = $(this).attr('data-url');
		var id = $(this).attr('data-photo-id');
		var that = $(this);

		$.ajax({
		  type: 'GET',
		  url: '/delete-ad-image',
		  data: {
		  	url: url,
		  	id: id
		  }, 
		  success: function(response) {
		  	if(response == 1){
		  		$(that).parent().remove();
		  	}
		  },


		});

	})

})